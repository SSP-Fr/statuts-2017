Guide de contribution
=====================

WorkFlow
--------

Ce projet utilise Git-flow au pied de la lettre:
* http://nvie.com/posts/a-successful-git-branching-model/

L'article de base qui donnera naissance au projet


* https://danielkummer.github.io/git-flow-cheatsheet/index.fr_FR.html

Aide mémoire français (et en d'autre traduction).



Contributions
-------------

Libre à vous de cloner le dépôt... Et de proposer des modifications.



