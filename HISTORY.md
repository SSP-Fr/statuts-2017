Historique du projet
====================

*Date de création*: 2017-10-23


Raisons de mise en place
------------------------



Événements importants
---------------------

### 2018-06-04
Envoi à l'imprimerie pour l'assemblée générale.


### 2018-02-26
Acceptation par le comité de région.


### 2017-10-23
Publication sur la forge.
Première version proposée au comité.


