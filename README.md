SSP Région Fribourg ; Statuts 2017
==================================

*date de création* : 2017/10/23


Destinataires du projet
-----------------------
* Les membres du SSP
* Le secrétariat régional


Ce que le projet fait
---------------------
Archive le travail effectué sur les statuts proposés en 2017 pour la région
Fribourg du SSP.



Prérequis
---------
### Matériels
Cela doit fonctionner aussi bien sur PC que sur Mac. Il est cependant évident que tout fonctionne bien mieux si vous utilisez GNU/Linux...


### Logiciel
Pour travailler aux statuts, un simple éditeur de texte suffit.

Pour compiler et produire le PDF, il faut avoir LaTeX.


Documentation
-------------
1. [CHANGELOG](CHANGELOG.md) Changements importants d'une version à l'autre
2. [CONTRIBUTING](CONTRIBUTING.md) indications utiles pour les personnes qui voudraient contribuer au projet
3. [CREDITS](CREDITS) liste des contributeurs du projet
4. [FAQ](FAQ.md) "Foire Aux Questions" pour le projet, au format texte
5. [HISTORY](HISTORY.md) histoire du projet
6. [INSTALL](INSTALL.md) instructions de configuration, de compilation et d'installation
7. [LICENSE](LICENSE) termes de la licence
8. [MANIFEST](MANIFEST) liste des fichiers
9. [NEWS](NEWS) dernières nouvelles


### Documentation généraliste
#### Git


#### GitFlow


#### LaTex


