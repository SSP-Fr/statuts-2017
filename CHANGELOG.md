Changelog
=========

* http://keepachangelog.com/fr/0.3.0/


## [Unreleased]


## [0.1] - 2018-06-04
### Changed
- Modifications faites par le comité de région.

### Added
- La version retravaillée par Gaétan à proposer au comité.
- Une première mise en page.
- Ajout de fichiers d'accompagnement GIT.

